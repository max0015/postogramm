require 'rails_helper'

RSpec.describe "Feeds", type: :request do
  describe "GET /index" do
    let!(:user) { create(:user) }
    let!(:followees) { create_list(:user, 3) }
    let!(:posts) do
      followees.each.collect do |f|
        create(:post, user: f)
      end
    end

    before :each do
      followees.each do |f|
        user.follow_user f
      end

      sign_in user
    end
    
    it 'returns http success' do
      get feed_url
      expect(response).to have_http_status(:success)
    end

    it 'shows posts' do
      get feed_url
      
      posts.each do |post|
        expect(response.body).to include(post.text)
      end
    end
  end
end
