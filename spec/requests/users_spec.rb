require 'rails_helper'
require 'cgi'

RSpec.describe '/users', type: :request do

  let(:user) { create(:user) }
  let(:posts) do
    3.times.collect { create(:post, user: user) }
  end

  describe 'GET /show' do
    it 'renders user page with posts' do
      posts

      get users_show_url(user)
      expect(response).to be_successful

      expect(response.body).to include(
        CGI.escapeHTML(user.full_name)
      )

      posts.each do |post|
        expect(response.body).to include(
          CGI.escapeHTML(post.text)
        )
      end
    end
  end

  describe 'GET /show_by_nickname' do
    it 'renders user page with posts' do
      posts

      get users_show_nickname_url(user.profile.nickname)
      expect(response).to be_successful

      expect(response.body).to include(
        CGI.escapeHTML(user.full_name)
      )

      posts.each do |post|
        expect(response.body).to include(
          CGI.escapeHTML(post.text)
        )
      end
    end
  end

  describe 'POST /follow' do
    let(:user) { create(:user) }
    let(:followee) { create(:user) }

    before do
      sign_in user
    end

    context 'not subscribed yet' do
      it 'follows user' do
        post users_follow_url(followee)
        expect(response).to redirect_to(users_show_url(followee))
        user.reload

        expect(user.followees).to include(followee)
      end
    end

    context 'already following user' do
      it 'says already following' do
        user.follow_user(followee)
        
        post users_follow_url(followee)
        expect(response).to redirect_to(users_show_url(followee))
        follow_redirect!
        expect(response.body).to include('already following')
      end
    end
  end

   describe 'POST /unfollow' do
    let(:user) { create(:user) }
    let(:followee) { create(:user) }

    before do
      sign_in user
    end

    context 'followed user' do
      it 'unfollows user' do
        user.follow_user(followee)
        post users_unfollow_url(followee)
        expect(response).to redirect_to(users_show_url(followee))
        user.reload

        expect(user.followees).to_not include(followee)
      end
    end

    context 'not following user' do
      it 'says not following' do
        post users_unfollow_url(followee)
        expect(response).to redirect_to(users_show_url(followee))
        follow_redirect!
        expect(response.body).to include('You are not following this user')
      end
    end
  end
end
