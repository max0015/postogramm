require 'rails_helper'

RSpec.describe '/post_comments', type: :request do
  let!(:target_post) { create(:post) }
  let!(:user) { create(:user) }

  let!(:valid_attributes) { { text: 'Some text', user: user } }
  let!(:invalid_attributes) { { text: nil } }

  before :each do
    sign_in user
  end

  describe 'GET /new' do
    it 'renders a successful response' do
      get new_post_comment_url(post_id: target_post.id)
      expect(response).to be_successful
    end
  end

  describe 'GET /edit' do
    it 'renders a successful response' do
      post_comment = target_post.comments.create!(text: 'Some text', user: user)
      get edit_post_comment_url(post_id: target_post.id, id: post_comment.id)
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new PostComment' do
        expect {
          url = post_comment_index_url(post_id: target_post)
          post url, params: { post_comment: valid_attributes }
        }.to change { target_post.comments.count }.by(1)
      end

      it 'redirects to the created post_comment' do
        post post_comment_index_url(post_id: target_post.id), params: { post_comment: valid_attributes }
        expect(response).to redirect_to(post_url(target_post))
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new PostComment' do
        expect {
          post post_comment_index_url(post_id: target_post.id), params: { post_comment: invalid_attributes }
        }.to change { target_post.comments.count }.by(0)
      end

      it 'renders a response with 422 status (i.e. to display the \'new\' template)' do
        post post_comment_index_url(post_id: target_post.id), params: { post_comment: invalid_attributes }
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'PATCH /update' do
    context 'with valid parameters' do
      let(:new_attributes) {
        { text: 'New text' }
      }

      it 'updates the requested post_comment' do
        post_comment = target_post.comments.create! valid_attributes
        patch post_comment_url(post_id: target_post.id, id: post_comment), params: { post_comment: new_attributes }
        post_comment.reload
        expect(post_comment.text).to eq(new_attributes[:text])
      end
    end

    context 'with invalid parameters' do
      it 'renders a response with 422 status (i.e. to display the \'edit\' template)' do
        post_comment = target_post.comments.create! valid_attributes
        patch post_comment_url(post_id: target_post, id: post_comment), params: { post_comment: invalid_attributes }
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested post_comment' do
      post_comment = target_post.comments.create! valid_attributes
      expect {
        delete post_comment_url(post_id: target_post, id: post_comment)
      }.to change(PostComment, :count).by(-1)
    end
  end
end
