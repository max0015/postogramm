require 'rails_helper'

RSpec.describe '/posts', type: :request do
  let(:user) { create(:user) }

  before :each do
    sign_in user
  end

  describe 'GET /index' do
    before do
      sign_out :user
    end

    let(:post) { build(:post) }
    subject { response }

    it 'renders a successful response' do
      post.save

      get posts_url
      expect(subject).to be_successful
      expect(subject.body).to include(post.text)
    end
  end

  describe 'GET /show' do
    before do
      sign_out :user
    end

    let(:post) { build(:post) }
    subject { response }

    it 'renders a successful response' do
      post.save

      get post_url(post)
      expect(response).to be_successful
      expect(subject.body).to include(post.text)
    end
  end

  describe 'GET /new' do
    context 'not logged in' do
      before do
        sign_out :user
      end

      it 'redirects to login' do
        get new_post_url
        expect(response).not_to be_successful
        expect(response.redirect_url).to eq(new_user_session_url)
      end
    end

    context 'logged in' do
      it 'renders successful response' do
        get new_post_url
        expect(response).to be_successful
        expect(response.body).to include('New post')
      end
    end
  end

  describe 'GET /edit' do
    let(:post) { create(:post, user: user) }
    subject { response }

    context 'not logged in' do
      before do
        sign_out :user
      end

      it 'redirects to login' do
        get edit_post_url(post)
        expect(response).not_to be_successful
        expect(response.redirect_url).to eq(new_user_session_url)
      end
    end

    it 'renders a successful response' do
      get edit_post_url(post)
      expect(subject).to be_successful
    end
  end

  describe 'POST /create' do
    let(:valid_attributes) { attributes_for(:post) }
    let(:invalid_attributes) { { text: nil } }

    context 'with valid parameters' do
      it 'creates a new Post' do
        expect {
          post posts_url, params: { post: valid_attributes }
        }.to change(Post, :count).by(1)
      end

      it 'redirects to the created post' do
        post posts_url, params: { post: valid_attributes }
        expect(response).to redirect_to(post_url(Post.last))
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new Post' do
        expect {
          post posts_url, params: { post: invalid_attributes }
        }.to change(Post, :count).by(0)
      end

      it 'renders a response with 422 status (i.e. to display the new template)' do
        post posts_url, params: { post: invalid_attributes }
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context 'not logged in' do
      before do
        sign_out :user
      end

      it 'responds with error' do
        expect {
          post posts_url, params: { post: valid_attributes }
        }.to change(Post, :count).by(0)
        expect(response).not_to be_successful
      end
    end
  end

  describe 'PATCH /update' do
    let(:new_attributes) { attributes_for(:post) }
    let(:invalid_attributes) { { text: nil } }

    let(:post) { create(:post, user: user) }
    subject { post }

    context 'own post' do
      context 'with valid parameters' do
        it 'updates the requested post' do
          patch post_url(post), params: { post: new_attributes }
          subject.reload

          expect(subject.text).to eq(new_attributes[:text])
        end

        it 'redirects to the post' do
          patch post_url(post), params: { post: new_attributes }
          post.reload
          expect(response).to redirect_to(post_url(post))
        end
      end

      context 'with invalid parameters' do
        it 'renders a response with 422 status (i.e. to display the edit template)' do
          patch post_url(post), params: { post: invalid_attributes }
          expect(response).to have_http_status(:unprocessable_entity)
          expect(subject.text).not_to eq(invalid_attributes[:text])
        end
      end
    end

    context 'other user`s post' do
      let(:other_post) { create(:post) }

      it 'renders error' do
        patch post_url(other_post), params: { post: new_attributes }
        subject.reload

        expect(response).not_to be_successful
        expect(subject.text).not_to eq(new_attributes[:text])
      end
    end

    context 'not logged in' do
      before do
        sign_out :user
      end

      it 'responds with error' do
        patch post_url(post), params: { post: new_attributes }
        post.reload
        expect(response).not_to be_successful
        expect(subject.text).not_to eq(new_attributes[:text])
      end
    end
  end

  describe 'DELETE /destroy' do
    let(:post) { build(:post, user: user) }

    context 'own post' do
      it 'destroys the requested post' do
        post.save
        expect {
          delete post_url(post)
        }.to change(Post, :count).by(-1)
      end

      it 'redirects to the posts list' do
        post.save
        delete post_url(post)
        expect(response).to redirect_to(posts_url)
      end
    end

    context 'other user`s post' do
      let(:other_post) { build(:post) }

      it 'renders error' do
        other_post.save
        expect {
          delete post_url(other_post)
        }.to change(Post, :count).by(0)
        expect(response).not_to be_successful
      end
    end
  end

  describe 'POST /like' do
    let(:user) { create(:user) }
    let(:target_post) { create(:post) }

    before do
      sign_in user
    end

    context 'with return url' do
      it 'redirects to url' do
        post like_post_url(target_post, url: '/test')
        expect(response).to redirect_to('/test')
      end
    end

    context 'not liked yet' do
      it 'likes post' do
        expect {
          post like_post_url(target_post)
          target_post.reload
        }.to change(target_post, :likes_count).by(1)

        expect(response).to redirect_to(post_url(target_post))
      end
    end

    context 'already liked post' do
      it 'changes nothing' do
        target_post.set_like(user)

        expect {
          post like_post_url(target_post)
          target_post.reload
        }.to change(target_post, :likes_count).by(0)

        expect(response).to redirect_to(post_url(target_post))
      end
    end
  end

  describe 'POST /unlike' do
    let(:user) { create(:user) }
    let(:target_post) { create(:post) }

    before do
      sign_in user
    end

    context 'with return url' do
      it 'redirects to url' do
        post unlike_post_url(target_post, url: '/test')
        expect(response).to redirect_to('/test')
      end
    end

    context 'not liked yet' do
      it 'changes nothing' do
        expect {
          post unlike_post_url(target_post)
          target_post.reload
        }.to change(target_post, :likes_count).by(0)

        expect(response).to redirect_to(post_url(target_post))
      end
    end

    context 'already liked post' do
      it 'unlikes post' do
        target_post.set_like(user)

        expect {
          post unlike_post_url(target_post)
          target_post.reload
        }.to change(target_post, :likes_count).by(-1)

        expect(response).to redirect_to(post_url(target_post))
      end
    end
  end
end
