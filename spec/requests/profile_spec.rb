require 'rails_helper'

RSpec.describe '/users/profile', type: :request do
  let!(:user) { create(:user) }

  before :each do
    sign_in user
  end

  describe 'POST /update' do
    let(:new_attributes) { attributes_for(:profile) }
    let(:invalid_attributes) { { first_name: nil } }

    subject { user.profile }

    context 'with valid parameters' do
      it 'updates profile' do
        post users_profile_url, params: { profile: new_attributes }
        expect(response).to redirect_to(users_profile_url)

        subject.reload
        expect(subject.first_name).to eq(new_attributes[:first_name])
        expect(subject.last_name).to eq(new_attributes[:last_name])
        expect(subject.nickname).to eq(new_attributes[:nickname])
      end
    end

    context 'with invalid parameters' do
      it 'renders a response with 422 status (i.e. to display the edit template)' do
        post users_profile_url, params: { profile: invalid_attributes }
        expect(response).to have_http_status(:unprocessable_entity)

        subject.reload
        expect(subject.first_name).not_to eq(invalid_attributes[:first_name])
      end
    end

    context 'not logged in' do
      before do
        sign_out :user
      end

      it 'responds with error' do
        post users_profile_url, params: { profile: new_attributes }
        expect(response).not_to be_successful
        expect(subject.first_name).not_to eq(new_attributes[:first_name])
      end
    end
  end
end
