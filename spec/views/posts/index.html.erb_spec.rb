require 'rails_helper'
require 'cgi'

RSpec.describe 'posts/index', type: :view do
  let(:posts) { create_list(:post, 3) }

  before(:each) do
    assign(:posts, Post.where(id: posts).paginate(page: 1))
  end

  it 'renders a list of posts' do
    render
    posts.each do |post|
      expect(rendered).to include(
        CGI.escapeHTML(post.text)
      )

      expect(rendered).to include(
        CGI.escapeHTML(post.user.full_name)
      )
    end
  end
end
