require 'rails_helper'
require 'cgi'

RSpec.describe 'posts/show', type: :view do
  let(:post) { create(:post) }
  before(:each) do
    assign(:post, post)
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to include(
      CGI.escapeHTML(post.text)
    )
    expect(rendered).to include(
      CGI.escapeHTML(post.user.full_name)
    )
    expect(rendered).to include(post.created_at.to_s)
  end
end
