require 'rails_helper'

RSpec.describe 'posts/new', type: :view do
  let(:post) { build(:post) }

  before(:each) do
    assign(:post, post)
  end

  it 'renders the new post form' do
    render
    assert_select 'form[action=?][method=?]', posts_path, 'post' do
      assert_select 'textarea[name=?]', 'post[text]'

      assert_select 'input[name=?]', 'post[media]'
    end
  end
end
