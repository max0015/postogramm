require 'rails_helper'

RSpec.describe 'post_comments/edit', type: :view do
  let(:post_comment) { create(:post_comment) }

  before(:each) do
    assign(:comment, post_comment)
  end

  it 'renders the edit post_comment form' do
    render

    assert_select 'form[action=?][method=?]', post_comment_path(post_id: post_comment.post, id: post_comment), 'post' do
      assert_select 'textarea[name=?]', 'post_comment[text]'
    end
  end
end
