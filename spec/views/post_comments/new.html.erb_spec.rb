require 'rails_helper'

RSpec.describe 'post_comments/new', type: :view do
  let(:comment) { build(:post_comment, post: create(:post)) }
  before(:each) do
    assign(:comment, comment)
    @post = comment.post
  end

  it 'renders new post_comment form' do
    render

    assert_select 'form[action=?][method=?]', post_comment_index_path(post_id: comment.post), 'post' do
      assert_select 'textarea[name=?]', 'post_comment[text]'
    end
  end
end
