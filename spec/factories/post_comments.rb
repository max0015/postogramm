require 'faker'

FactoryBot.define do
  factory :post_comment do
    association :post
    association :user
    
    text { Faker::Lorem.paragraph }
  end
end
