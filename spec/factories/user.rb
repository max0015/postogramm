require 'faker'

FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    password { 'password' }
    password_confirmation { 'password' }
    profile { build(:profile, user: nil) }

    after(:create) do |user, evaluator|
      unless evaluator.profile.nil?
        evaluator.profile.user_id = user.id
        evaluator.profile.save
      end
    end
  end
end
