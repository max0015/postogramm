require 'faker'

FactoryBot.define do
  factory :profile do
    association :user
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    nickname do
      Faker::Internet.username(
        specifier: 5..10,
        separators: ['-', '_', '.']
      )
    end
  end
end
