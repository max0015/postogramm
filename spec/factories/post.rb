require 'faker'

FactoryBot.define do
  factory :post do
    association :user
    text { Faker::Lorem.paragraph }
    media_data { ShrineImageData.image_data }
  end
end
