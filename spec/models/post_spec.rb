require 'rails_helper'

RSpec.describe Post, type: :model do
  context 'with factory-bot data' do
    let(:post) { create(:post) }
    subject { post.save }

    it 'saves' do
      expect(subject).to eq(true)
    end
  end

  context 'with long text' do
    let(:user) { create(:user) }
    let(:post) do
      Post.new(
        user: user,
        text: Faker::Lorem.paragraph_by_chars(number: 70_000)
      )
    end
    subject { post.save }

    it 'not saves' do
      expect(post).to be_invalid
      expect(post.errors[:text]).to be_present
      expect(subject).to eq(false)
    end
  end

  context 'with image' do
    let(:file) { File.open('spec/files/test.png', binmode: true) }

    after do
      file_system = Shrine.storages[:cache]
      file_system.clear!
    end

    it 'can attach image' do
      post = Post.create(
        text: 'Some Text',
        user: create(:user),
        media: file
      )
      expect(post.media.storage_key).to eq(:store)
      post.update(media: nil) # Delete file
    end
  end

  describe '#liked?' do
    let(:post) { create(:post) }
    let(:user) { create(:user) }    
    
    subject { post.liked? user }
    
    context 'not like yet' do
      it 'returns false' do
        expect(subject).to eq(false)
      end
    end

    context 'has liked' do
      it 'return true' do
        post.set_like user
        expect(subject).to eq(true)
      end
    end
  end  

  describe '#set_like' do  
    let(:post) { create(:post) }
    let(:user) { create(:user) }
    
    subject { post.set_like user }
    
    context 'not liked yet' do
      it 'adds like' do
        expect { subject }.to change { post.likes_count }.by(1)
      end
    end

    context 'already liked' do
      it 'does nothing' do
        PostLike.create!(post: post, user: user)
        expect { subject }.to change { post.likes_count }.by(0)
      end
    end
  end

  
  describe '#remove_like' do  
    let(:post) { create(:post) }
    let(:user) { create(:user) }
    
    subject { post.remove_like user }
    
    context 'not liked yet' do
      it 'does nothing' do
        expect { subject }.to change { post.likes_count }.by(0)
      end
    end

    context 'already liked' do
      it 'removes like' do
        PostLike.create!(post: post, user: user)
        expect { subject }.to change { post.likes_count }.by(-1)
      end
    end
  end
end
