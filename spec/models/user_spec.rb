require 'rails_helper'

RSpec.describe User, type: :model do
  context 'without email' do
    let(:user) { User.new }
    subject { user.save }

    it 'does not saves' do
      expect(user).to be_invalid
      expect(user.errors[:email]).to be_present

      expect(subject).to eq(false)
    end
  end

  context 'with email and password' do
    let(:user) do
      User.new(
        email: 'test@mail.ru',
        password: 'password',
        password_confirmation: 'password'
      )
    end
    subject { user.save }

    it 'saves' do
      expect(user).to be_valid
      expect(subject).to eq(true)
    end
  end

  describe '#following?' do
    let(:user) { create(:user) }
    let(:followee) { create(:user) }

    subject { user.following? followee }

    context 'user is followed' do
      it 'returns true' do
        user.follow_user followee
        expect(subject).to eq(true)
      end
    end

    context 'user is not followed' do
      it 'returns false' do
        expect(subject).to eq(false)
      end
    end
  end

  describe '#follow_user' do
    let(:user) { create(:user) }
    let(:followee) { create(:user) }

    subject { user.follow_user followee }

    it 'adds relation' do
      subject
      expect(user.followees).to include(followee)
    end
  end

  describe '#unfollow_user' do
    let(:user) { create(:user) }
    let(:followee) { create(:user) }

    subject { user.unfollow_user followee }
    
    before do
      user.followees << followee
    end
    
    it 'removes relation' do
      subject
      expect(user.followees).to_not include(followee)
    end
  end

  describe '#full_name' do
    let(:user) do 
      build(:user, profile: build(
        :profile, 
        first_name: 'John', 
        last_name: 'Doe'
      ))
    end

    subject { user.full_name }

    it 'returns full name' do
      expect(subject).to eq('John Doe')
    end
  end
end
