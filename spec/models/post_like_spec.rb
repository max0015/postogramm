require 'rails_helper'

RSpec.describe PostLike, type: :model do
  context 'without user' do
    let(:post) { create(:post) }
    let(:like) { PostLike.new(post: post) }

    subject { like.save }
    it 'not saves' do
      expect(subject).to_not eq(true)
    end
  end

  context 'without post' do
    let(:user) { create(:user) }
    let(:like) { PostLike.new(user: user) }

    subject { like.save }
    it 'not saves' do
      expect(subject).to_not eq(true)
    end
  end

  context 'with valid user and post' do
    let(:post) { create(:post) }
    let(:user) { create(:user) }
    let(:like) { PostLike.new(post: post, user: user) }

    subject { like.save }
    it 'saves' do
      expect(subject).to eq(true)
    end

    it 'increments post likes counter' do
      expect { subject }.to change(post, :likes_count).by(1)
    end
  end
end
