require 'rails_helper'

RSpec.describe Profile, type: :model do
  context 'without first_name' do
    let(:profile) { Profile.new }
    subject { profile.save }

    it 'does not saves' do
      expect(profile).to be_invalid
      expect(profile.errors[:first_name]).to be_present

      expect(subject).to eq(false)
    end
  end

  context 'with valid data' do
    let(:user) { create(:user, profile: nil) }
    let(:profile) do
      Profile.new(
        user: user,
        first_name: 'John',
        last_name: 'Doe',
        nickname: 'john_doe'
      )
    end
    subject { profile.save }

    it 'saves' do
      expect(profile).to be_valid
      expect(subject).to eq(true)
    end
  end

  context 'nickname contains special chars' do
    let(:profile) { create(:profile) }
    subject { profile.save }

    it 'not saves' do
      profile.nickname = 'some_^*nickname'

      expect(profile).to be_invalid
      expect(profile.errors[:nickname]).to be_present
      expect(subject).to eq(false)
    end
  end

  context 'nickname contains only a-zA-Z0-9_-' do
    let(:profile) { create(:profile) }
    subject { profile.save }

    it 'saves' do
      profile.nickname = 'my_nick-name123'

      expect(profile).to be_valid
      expect(subject).to eq(true)
    end
  end

  context 'non-unique nickname' do
    let(:profile_one) { create(:profile) }
    let(:profile_two) { create(:profile) }
    subject { profile_two.save }

    it 'not saves' do
      profile_two.nickname = profile_one.nickname

      expect(profile_two).to be_invalid
      expect(profile_two.errors[:nickname]).to be_present
      expect(subject).to eq(false)
    end
  end
end
