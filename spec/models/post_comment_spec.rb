require 'rails_helper'

RSpec.describe PostComment, type: :model do
  
  context 'with factory-bot data' do
    let(:comment) { build(:post_comment) }
    subject { comment.save }

    it 'saves' do
      expect(subject).to eq(true)
    end
  end

  context 'with long text' do
    let(:comment) { build(
      :post_comment, 
      text: Faker::Lorem.paragraph_by_chars(number: 70_000)
    ) }
    
    subject { comment.save }

    it 'not saves' do
      expect(comment).to be_invalid
      expect(comment.errors[:text]).to be_present
      expect(subject).to eq(false)
    end
  end
end
