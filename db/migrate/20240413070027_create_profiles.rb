class CreateProfiles < ActiveRecord::Migration[7.1]
  def change
    create_table :profiles do |t|
      t.references :user
      t.string :first_name, null: false
      t.string :last_name
      t.string :nickname

      t.timestamps
    end
  end
end
