class CreateUserFollowsTable < ActiveRecord::Migration[7.1]
  def change
    create_table :user_follows, id: false do |t|
      t.bigint :user_id
      t.bigint :subject_id

      t.references :user_id, foreign_key: { to_table: :users }
      t.references :subject_id, foreign_key: { to_table: :users }
      t.index [:user_id, :subject_id], unique: true
    end
  end
end
