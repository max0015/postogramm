Rails.application.routes.draw do
  devise_for :users, controllers: {
    registrations: 'users/registrations'
  }

  get 'users/profile', to: 'users/profile#edit'
  post 'users/profile', to: 'users/profile#update'

  resources :posts do
    post 'like', on: :member
    post 'unlike', on: :member

    resources :post_comments, path: 'comments', as: :comment
  end

  post 'users/:followee/follow', to: 'users#follow', as: :users_follow
  post 'users/:followee/unfollow', to: 'users#unfollow', as: :users_unfollow

  get 'users/@:nickname', to: 'users#show_by_nickname', as: :users_show_nickname
  get 'users/:id', to: 'users#show', as: :users_show
  
  get 'feed', to: 'feed#index'

  get "up" => "rails/health#show", as: :rails_health_check
  root "posts#index"
end
