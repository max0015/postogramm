class PostComment < ApplicationRecord
  belongs_to :post
  belongs_to :user
  
  validates :text, presence: true, length: {
    minimum: 2,
    maximum: 65_000
  }
end
