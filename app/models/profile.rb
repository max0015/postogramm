class Profile < ApplicationRecord
  validates :first_name, presence: true
  validates :nickname, uniqueness: true, format: {
    with: /\A[a-zA-Z0-9_\-.]+\z/,
    message: 'only latin letters, numbers, dots and dashes allowed'
  }, allow_blank: true

  belongs_to :user
end
