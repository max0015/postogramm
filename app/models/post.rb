class Post < ApplicationRecord
  include ShrineImageUploader::Attachment(:media)

  self.per_page = 10
  default_scope { order(created_at: :desc) }

  validates :text, presence: true, length: {
    minimum: 2,
    maximum: 65_000
  }

  belongs_to :user
  has_many :likes, class_name: 'PostLike', dependent: :destroy
  has_many :comments, class_name: 'PostComment', dependent: :destroy

  def liked?(user)
    likes.where(user: user).exists?
  end

  def set_like(user)
    unless liked? user
      likes.create(user: user) 
    end
  end

  def remove_like(user)
    like = likes.where(user: user).first
    like.destroy unless like.nil? 
  end

  def generate_thumbnails
    self.media_derivatives!
    self.save
  end
end
