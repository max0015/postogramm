class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :lockable

  validates :email, presence: true

  has_one :profile, autosave: true
  accepts_nested_attributes_for :profile

  has_many :posts, dependent: :destroy
  has_many :comments, class_name: 'PostComment', dependent: :destroy
  has_many :likes, class_name: 'PostLike', dependent: :destroy

  has_and_belongs_to_many(
    :followees,
    join_table: 'user_follows',
    class_name: 'User',
    association_foreign_key: 'subject_id'
  )

  def full_name
    "#{profile.first_name} #{profile.last_name}"
  end

  def following?(user)
    followees.include? user
  end

  def follow_user(user)
    followees << user
  end

  def unfollow_user(user)
    followees.delete(user)
  end
end
