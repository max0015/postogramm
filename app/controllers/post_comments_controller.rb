class PostCommentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_post, only: %i[ new create edit update destroy ]
  before_action :set_comment, only: %i[ edit update destroy ]

  # GET /post/:post_id/comments/new
  def new
    @comment = PostComment.new(post: @post, user: current_user)
  end

  # GET /post/:post_id/comments/1/edit
  def edit
  end

  # POST /post/:post_id/comments
  def create
    @comment = PostComment.new(comment_params)
    @comment.post = @post
    @comment.user = current_user
    
    if @comment.save
      redirect_to post_url(@post), notice: "Comment was successfully added"
    else
      render :new, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /post/:post_id/comments/1
  def update
    if @comment.update(comment_params)
      redirect_to post_url(@post), notice: "Comment was successfully updated."
    else
      render :edit, status: :unprocessable_entity
    end
  end

  # DELETE /post/:post_id/comments/1
  def destroy
    @comment.destroy!
    redirect_to post_url(@post), notice: "Post comment was successfully destroyed."
  end

  private
    def set_post
      @post = Post.find(params[:post_id])  
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = PostComment.find_by(id: params[:id], user: current_user)
    end

    # Only allow a list of trusted parameters through.
    def comment_params
      params.require(:post_comment).permit(:text)
    end
end
