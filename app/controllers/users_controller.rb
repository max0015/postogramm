class UsersController < ApplicationController
  before_action :authenticate_user!, only: %i[follow unfollow]
  before_action :set_followee, only: %i[follow unfollow]

  before_action :set_user, only: :show
  before_action :set_user_by_nickname, only: :show_by_nickname
  before_action :set_posts, only: %i[show show_by_nickname]

  def show
  end

  def show_by_nickname
    render 'show'
  end

  def follow
    if current_user.following?(@followee)
      redirect_to users_show_url(@followee), notice: 'You are already following this user'
    else
      current_user.follow_user(@followee)
      redirect_to users_show_url(@followee), notice: 'Great! Now you are following this user'
    end
  end

  def unfollow
    if current_user.following?(@followee)
      current_user.unfollow_user(@followee)
      redirect_to users_show_url(@followee), notice: 'Done. Now you are not following this user'
    else
      redirect_to users_show_url(@followee), notice: 'You are not following this user'
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def set_user_by_nickname
    profile = Profile.find_by(nickname: params[:nickname]) or not_found!
    @user = profile.user
  end

  def set_posts
    @posts = PostsHelper::load_for_user(@user).paginate(page: params[:page])
    @likes_map = PostsHelper::load_user_likes(current_user, @posts)
  end

  def set_followee
    @followee = User.find(params[:followee])
  end
end
