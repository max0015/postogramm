class PostsController < ApplicationController
  before_action :authenticate_user!, only: %i[ new edit update destroy like unlike ]
  before_action :set_post, only: %i[ show like unlike ]
  before_action :set_post_edit, only: %i[ edit update destroy ]

  # GET /posts or /posts.json
  def index
    @posts = PostsHelper::load_all.paginate(page: params[:page])
    @likes_map = PostsHelper::load_user_likes(current_user, @posts)
  end

  # GET /posts/1 or /posts/1.json
  def show
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts or /posts.json
  def create
    @post = Post.new(post_params)
    @post.user = current_user

    respond_to do |format|
      if @post.save
        @post.generate_thumbnails
        format.html { redirect_to post_url(@post), notice: "Post was successfully created." }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1 or /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        @post.generate_thumbnails
        format.html { redirect_to post_url(@post), notice: "Post was successfully updated." }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1 or /posts/1.json
  def destroy
    @post.destroy!

    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def like
    @post.set_like(current_user)
    return_url = post_url(@post)

    unless params[:url].nil?
      return_url = params[:url]
    end

    redirect_to return_url, notice: 'Great! You liked this post'
  end
  
  def unlike
    @post.remove_like(current_user)
    return_url = post_url(@post)

    unless params[:url].nil?
      return_url = params[:url]
    end

    redirect_to return_url, notice: 'Like removed'
  end

  private

  def set_post
    @post = Post.find(params[:id])
  end

  def set_post_edit
    @post = Post.find_by(
      id: params[:id],
      user: current_user
    ) or not_found!
  end

  def post_params
    params.require(:post).permit(:text, :media)
  end
end
