class FeedController < ApplicationController
  before_action :authenticate_user!
  
  def index
    @posts = PostsHelper::default_queryset
      .where(user: current_user.followees)
      .paginate(page: params[:page])
    
    @likes_map = PostsHelper::load_user_likes(current_user, @posts)
  end
end
