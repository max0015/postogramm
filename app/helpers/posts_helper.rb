module PostsHelper
  def post_belongs_to_me?(post)
    post.user == current_user
  end

  def post_liked?(post, user, map)
    return !map[post.id].nil? unless map.nil?
    post.liked?(user)
  end

  def self.default_queryset
    Post.includes(user: [:profile])
  end

  def self.load_all
    default_queryset.all
  end

  def self.load_for_user(user)
    default_queryset.where(user: user)
  end

  def self.load_user_likes(user, posts)
    likes_map = {}
    unless user.nil?
      PostLike.where(user: user, post_id: posts.ids).each do |like|
        likes_map[like.post_id] = like  
      end
    end
      
    likes_map
  end

  def post_return_url(post)
    request.original_url + "##{dom_id post}"
  end

  def preview_image_url(post)
    return nil if post.media_data.nil?

    thumbnail = post.media(:small)
    return thumbnail.url unless thumbnail.nil?

    return post.media_url
  end
end
