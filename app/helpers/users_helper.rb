module UsersHelper
  def user_url(user)
    if !user.profile.nickname.nil? && !user.profile.nickname.empty?
      users_show_nickname_url(user.profile.nickname)
    else
      users_show_url(user.id)
    end
  end
end
