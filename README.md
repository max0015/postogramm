# Post-o-gramm

Things to cover:

* Uses ruby-3.0.2

* Database: sqlite3, run `rails db:migrate`


Features:
* Users can sign-up, sign-in, update profile
* Users can create posts with image
* Users can like / unlike posts
* Users can see likes count on post
* Users can leave comments on posts
* Users can edit / remove own comments
* Users can follow / unfollow other users
* Users can view feed, made of followee posts
